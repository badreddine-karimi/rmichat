package com.karimi.client;

import com.karimi.server.IChatServer;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;

public class MainClient {

    public static void main(String[] args) throws RemoteException, NotBoundException, MalformedURLException {

        String serverURL ="//localhost:8079/RMIChat";

        IChatServer chatServer = (IChatServer) Naming.lookup(serverURL);

        try {
            new Thread(new ChatClient(args[0],chatServer)).start();
        }catch (Exception e){
            e.printStackTrace();
        }

    }
}
